import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CurrentUserComponent } from './components/user-components/current-user/current-user.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { AdminListComponent } from './components/user-components/admin-list/admin-list.component';
import { TeacherListComponent } from './components/user-components/teacher-list/teacher-list.component';
import { ParentListComponent } from './components/user-components/parent-list/parent-list.component';
import { StudentListComponent } from './components/user-components/student-list/student-list.component';
import { AdminUserComponent } from './components/user-components/admin-user/admin-user.component';
import { TeacherUserComponent } from './components/user-components/teacher-user/teacher-user.component';
import { ParentUserComponent } from './components/user-components/parent-user/parent-user.component';
import { StudentUserComponent } from './components/user-components/student-user/student-user.component';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectDetailComponent } from './components/subject-detail/subject-detail.component';
import { MarkListComponent } from './components/mark-list/mark-list.component';
import { MarkDetailComponent } from './components/mark-detail/mark-detail.component';
import { AdminAddComponent } from './components/user-components/admin-add/admin-add.component';
import { TeacherAddComponent } from './components/user-components/teacher-add/teacher-add.component';
import { ParentAddComponent } from './components/user-components/parent-add/parent-add.component';
import { StudentAddComponent } from './components/user-components/student-add/student-add.component';
import { SubjectAddComponent } from './components/subject-add/subject-add.component';
import { MarkAddComponent } from './components/mark-add/mark-add.component';
import { UserEditComponent } from './components/user-components/user-edit/user-edit.component';
import { AdminEditComponent } from './components/user-components/admin-edit/admin-edit.component';
import { TeacherEditComponent } from './components/user-components/teacher-edit/teacher-edit.component';
import { ParentEditComponent } from './components/user-components/parent-edit/parent-edit.component';
import { StudentEditComponent } from './components/user-components/student-edit/student-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrentUserComponent,
    LoginComponent,
    AdminListComponent,
    TeacherListComponent,
    ParentListComponent,
    StudentListComponent,
    AdminUserComponent,
    TeacherUserComponent,
    ParentUserComponent,
    StudentUserComponent,
    SubjectListComponent,
    SubjectDetailComponent,
    MarkListComponent,
    MarkDetailComponent,
    AdminAddComponent,
    TeacherAddComponent,
    ParentAddComponent,
    StudentAddComponent,
    SubjectAddComponent,
    MarkAddComponent,
    UserEditComponent,
    AdminEditComponent,
    TeacherEditComponent,
    ParentEditComponent,
    StudentEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
