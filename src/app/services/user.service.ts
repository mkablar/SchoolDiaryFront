import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from '../../../node_modules/rxjs';
import { PublicUser } from '../models/PublicUser';
import { CurrentUserService } from './current-user.service';
import { AdminUser } from '../models/AdminUser';
import { TeacherUser } from '../models/TeacherUser';
import { ParentUser } from '../models/ParentUser';
import { StudentUser } from '../models/StudentUser';
import { CreateAdultUser } from '../models/CreateAdultUser';
import { CreateStudent } from '../models/CreateStudent';
import { catchError, tap } from '../../../node_modules/rxjs/operators';
import { UpdateAdultUser } from '../models/UpdateAdultUser';
import { UpdateStudent } from '../models/UpdateStudent';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private productsUrl = 'http://localhost:53025/api/';
  private accountsUrl = 'http://localhost:53025/api/account/';
  private adminsUrl = `${this.productsUrl}admins/`;
  private teachersUrl = `${this.productsUrl}teachers/`;
  private parentsUrl = `${this.productsUrl}parents/`;
  private studentsUrl = `${this.productsUrl}students/`;

  constructor(private http: HttpClient, private currentUserService: CurrentUserService) {
  }

  getAdmins(): Observable<PublicUser[]> {
    // return this.http.get<PublicUser[]>(this.adminsUrl, this.currentUserService.getOptionsWithToken());
    return this.http.get<PublicUser[]>(this.adminsUrl);
  }

  getTeachers(): Observable<PublicUser[]> {
    // return this.http.get<PublicUser[]>(this.teachersUrl, this.currentUserService.getOptionsWithToken());
    return this.http.get<PublicUser[]>(this.teachersUrl);
  }

  getParents(): Observable<PublicUser[]> {
    // return this.http.get<PublicUser[]>(this.parentsUrl, this.currentUserService.getOptionsWithToken());
    return this.http.get<PublicUser[]>(this.parentsUrl);
  }

  getStudents(): Observable<PublicUser[]> {
    // return this.http.get<PublicUser[]>(this.studentsUrl, this.currentUserService.getOptionsWithToken());
    return this.http.get<PublicUser[]>(this.studentsUrl);
  }

  getUserAdmin(id: string): Observable<AdminUser> {
    // let role: string = this.currentUserService.currentUser.role;
    let role = 'admins';
    let url = this.adminsUrl;
    switch (role) {
      case 'admins': {
        url = `${url}admin/`;
        break;
      }
      case 'teachers': {
        url = `${url}teacher/`;
        break;
      }
      default: {
        break;
      }
    }
    return this.http.get<AdminUser>(`${url}${id}`);
  }

  getUserTeacher(id: string): Observable<TeacherUser> {
    // let role: string = this.currentUserService.currentUser.role;
    let role = 'admins';
    let url = this.teachersUrl;
    switch (role) {
      case 'admins': {
        url = `${url}admin/`;
        break;
      }
      case 'teachers': {
        url = `${url}teacher/`;
        break;
      }
      default: {
        break;
      }
    }
    return this.http.get<TeacherUser>(`${url}${id}`);
  }

  getUserParent(id: string): Observable<ParentUser> {
    // let role: string = this.currentUserService.currentUser.role;
    let role = 'admins';
    let url = this.parentsUrl;
    switch (role) {
      case 'admins': {
        url = `${url}admin/`;
        break;
      }
      case 'teachers': {
        url = `${url}teacher/`;
        break;
      }
      default: {
        break;
      }
    }
    return this.http.get<ParentUser>(`${url}${id}`);
  }

  getUserStudent(id: string): Observable<StudentUser> {
    // let role: string = this.currentUserService.currentUser.role;
    let role = 'admins';
    let url = this.studentsUrl;
    switch (role) {
      case 'admins': {
        url = `${url}admin/`;
        break;
      }
      case 'teachers': {
        url = `${url}teacher/`;
        break;
      }
      default: {
        break;
      }
    }
    return this.http.get<StudentUser>(`${url}${id}`);
  }

  addAdmin(admin: CreateAdultUser): Observable<CreateAdultUser> {
    return this.http.post<CreateAdultUser>(`${this.accountsUrl}register-admin`, admin);//, this.currentUserService.getOptionsWithToken());
  }

  addTeacher(teacher: CreateAdultUser): Observable<CreateAdultUser> {
    return this.http.post<CreateAdultUser>(`${this.accountsUrl}register-teacher`, teacher); //, this.currentUserService.getOptionsWithToken());
  }

  addParent(parent: CreateAdultUser): Observable<CreateAdultUser> {
    return this.http.post<CreateAdultUser>(`${this.accountsUrl}register-parent`, parent);//, this.currentUserService.getOptionsWithToken());
  }

  addStudent(student: CreateStudent): Observable<CreateStudent> {
    return this.http.post<CreateStudent>(`${this.accountsUrl}register-student`, student);//, this.currentUserService.getOptionsWithToken())
  }

  editAdmin(admin: UpdateAdultUser): Observable<UpdateAdultUser> {
    return this.http.put<UpdateAdultUser>(`${this.adminsUrl}${admin.id}`, admin);
  }

  editTeacher(teacher: UpdateAdultUser): Observable<UpdateAdultUser> {
    return this.http.put<UpdateAdultUser>(`${this.teachersUrl}${teacher.id}`, teacher);
  }

  editParent(parent: UpdateAdultUser): Observable<UpdateAdultUser> {
    return this.http.put<UpdateAdultUser>(`${this.parentsUrl}${parent.id}`, parent);
  }

  editStudent(student: UpdateStudent): Observable<UpdateStudent> {
    return this.http.put<UpdateStudent>(`${this.studentsUrl}${student.id}`, student);
  }



  searchAdmins(term: string): Observable<PublicUser[]> {
    return this.http.get<PublicUser[]>(`${this.adminsUrl}?temp=${term}`).pipe(tap());
  }
}
