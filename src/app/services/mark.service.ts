import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { CurrentUserService } from './current-user.service';
import { Observable } from '../../../node_modules/rxjs';
import { PublicMark } from '../models/PublicMark';
import { Mark } from '../models/Mark';
import { StudentSubjectMarks } from '../models/StudentSubjectMarks';
import { CreateMark } from '../models/CreateMark';

@Injectable({
  providedIn: 'root'
})
export class MarkService {
  private marksUrl = 'http://localhost:53025/api/marks/';
  private studentsUrl = 'http://localhost:53025/api/students/';

  constructor(private http: HttpClient, private currentUserService: CurrentUserService) { }

  getMarks(id: string): Observable<StudentSubjectMarks[]> {
    return this.http.get<StudentSubjectMarks[]>(`${this.studentsUrl}${id}/getmarks`);
  }

  getMark(id: number): Observable<Mark> {
    return this.http.get<Mark>(`${this.marksUrl}${id}`);
  }

  addMark(mark: CreateMark): Observable<CreateMark> {
    return this.http.post<CreateMark>(this.marksUrl, mark, this.currentUserService.getOptionsWithToken());
  }
}
