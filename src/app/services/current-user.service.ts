import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CurrentUser } from '../models/CurrentUser';
import { Observable } from '../../../node_modules/rxjs';
import { tap } from '../../../node_modules/rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {
  currentUser: CurrentUser;

  private loginUrl = 'http://localhost:53025/token';

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<CurrentUser> {
    const body = new HttpParams()
      .set('grant_type', 'password')
      .set('username', username)
      .set('password', password);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return this.http.post<CurrentUser>(this.loginUrl, body.toString(), httpOptions)
      .pipe(
        tap(currentUser => this.currentUser = currentUser)
      );
  }

  logout() {
    this.currentUser = null;
  }

  getOptionsWithToken() {
    return {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.currentUser.access_token}`
      })
    };
  }
}
