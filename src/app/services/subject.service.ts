import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { CurrentUserService } from './current-user.service';
import { Observable } from '../../../node_modules/rxjs';
import { Subject } from '../models/Subject';
import { CreateSubject } from '../models/CreateSubject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private subjectUrl = 'http://localhost:53025/api/subjects/';

  constructor(private http: HttpClient, private currentUserService: CurrentUserService) { }

  getSubjects(): Observable<Subject[]> {
    // return this.http.get<PublicUser[]>(this.productsUrl+'admins', this.currentUserService.getOptionsWithToken());
    return this.http.get<Subject[]>(this.subjectUrl);
  }

  getSubject(id: number): Observable<Subject> {
    return this.http.get<Subject>(`${this.subjectUrl}${id}`);
  }

  addSubject(subject: CreateSubject): Observable<CreateSubject> {
    return this.http.post<CreateSubject>(this.subjectUrl, subject);
  }
}
