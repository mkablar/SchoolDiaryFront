import { Component, OnInit } from '@angular/core';
import { CreateMark } from '../../models/CreateMark';
import { MarkService } from '../../services/mark.service';

@Component({
  selector: 'app-mark-add',
  templateUrl: './mark-add.component.html',
  styleUrls: ['./mark-add.component.css']
})
export class MarkAddComponent implements OnInit {
  mark: CreateMark;

  constructor(private markService: MarkService) {
    this.mark = new CreateMark();
   }

  ngOnInit() {
  }

  addMark(value: number, description: string) {
    this.mark.value = value;
    this.mark.description = description;

    this.markService.addMark(this.mark).subscribe();

    return false;
  }

}
