import { Component, OnInit, Input } from '@angular/core';
import { Mark } from '../../models/Mark';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { MarkService } from '../../services/mark.service';
import { Location } from '../../../../node_modules/@angular/common';

@Component({
  selector: 'app-mark-detail',
  templateUrl: './mark-detail.component.html',
  styleUrls: ['./mark-detail.component.css']
})
export class MarkDetailComponent implements OnInit {
  @Input() mark: Mark;

  constructor(private route: ActivatedRoute, private markService: MarkService, private location: Location) { }

  ngOnInit() {
    this.getMark();
  }

  getMark(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.markService.getMark(id).subscribe(mark => this.mark = mark);
  }

}
