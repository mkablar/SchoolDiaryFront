import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentUserService } from '../../services/current-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;

  constructor(private router: Router, private currentUserService: CurrentUserService) { }

  ngOnInit() {
    this.currentUserService.logout();
  }

  login() {
    this.loading = true;
    this.currentUserService
      .login(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.loading = false;
          if (data) {
            this.router.navigate(['/admins']);
          } else {
            console.error(data);
          }
        },
        error => {
          console.error(error);
          this.loading = false;
        }
      );
  }

}
