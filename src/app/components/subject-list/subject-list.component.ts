import { Component, OnInit } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { Subject } from '../../models/Subject';
import { SubjectService } from '../../services/subject.service';

@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {
  subjects$: Observable<Subject[]>;

  constructor(private subjectService: SubjectService) { }

  ngOnInit() {
    this.subjects$ = this.subjectService.getSubjects();
  }

}
