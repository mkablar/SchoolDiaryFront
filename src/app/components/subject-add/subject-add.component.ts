import { Component, OnInit } from '@angular/core';
import { CreateSubject } from '../../models/CreateSubject';
import { SubjectService } from '../../services/subject.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-subject-add',
  templateUrl: './subject-add.component.html',
  styleUrls: ['./subject-add.component.css']
})
export class SubjectAddComponent implements OnInit {
  subject: CreateSubject;

  constructor(private subjectService: SubjectService, private router: Router) {
    this.subject = new CreateSubject();
   }

  ngOnInit() {
  }

  addSubject(subjectName: string, weeklySubjectFund: number, description: string) {
    this.subject.name = subjectName;
    this.subject.weeklySubjectFund = weeklySubjectFund;
    this.subject.description = description;

    this.subjectService.addSubject(this.subject).subscribe((subject: CreateSubject) => {
      this.router.navigate(['/subjects']);
    });

    return false;
  }
}
