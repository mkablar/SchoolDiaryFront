import { Component, OnInit } from '@angular/core';
import { CreateStudent } from '../../../models/CreateStudent';
import { UserService } from '../../../services/user.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {
  student: CreateStudent;

  constructor(private userService: UserService, private router: Router) {
    this.student = new CreateStudent();
   }

  ngOnInit() {
  }

  addStudent(userName: string, firstName: string, lastName: string, password: string, confirmPassword: string,
    address: string) {
     this.student.userName = userName;
     this.student.firstName = firstName;
     this.student.lastName = lastName;
     this.student.password = password;
     this.student.confirmPassword = confirmPassword;
     this.student.userRole = 'students';
     this.student.address = address;

     this.userService.addStudent(this.student).subscribe((student: CreateStudent) => {
       this.router.navigate(['/students']);
     });

     return false;
    }

}
