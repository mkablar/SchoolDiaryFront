import { Component, OnInit } from '@angular/core';
import { Observable } from '../../../../../node_modules/rxjs';
import { PublicUser } from '../../../models/PublicUser';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-parent-list',
  templateUrl: './parent-list.component.html',
  styleUrls: ['./parent-list.component.css']
})
export class ParentListComponent implements OnInit {
  users$: Observable<PublicUser[]>;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.users$ = this.userService.getParents();
  }

}
