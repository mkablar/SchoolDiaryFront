import { Component, OnInit } from '@angular/core';
import { Observable } from '../../../../../node_modules/rxjs';
import { PublicUser } from '../../../models/PublicUser';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {
  users$: Observable<PublicUser[]>;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.users$ = this.userService.getTeachers();
  }

}
