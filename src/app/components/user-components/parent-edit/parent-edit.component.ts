import { Component, OnInit } from '@angular/core';
import { ParentUser } from '../../../models/ParentUser';
import { UpdateAdultUser } from '../../../models/UpdateAdultUser';
import { UserService } from '../../../services/user.service';
import { Location } from '../../../../../node_modules/@angular/common';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-parent-edit',
  templateUrl: './parent-edit.component.html',
  styleUrls: ['./parent-edit.component.css']
})
export class ParentEditComponent implements OnInit {
  user: ParentUser;
  parent: UpdateAdultUser;

  constructor(private userService: UserService, private location: Location, private route: ActivatedRoute) {
    this.parent = new UpdateAdultUser();
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserParent(id).subscribe(user => this.user = user);
  }

  editParent(id: string, userName: string, firstName: string, lastName: string, address: string,
    phoneNumber: string, email: string) {
    this.parent.id = id != '' ? id : this.user.id;
    this.parent.userName = userName != '' ? userName : this.user.userName;
    this.parent.firstName = firstName != '' ? firstName : this.user.firstName;
    this.parent.lastName = lastName != '' ? lastName : this.user.lastName;
    this.parent.address = address != '' ? address : this.user.address;
    this.parent.phoneNumber = phoneNumber != '' ? phoneNumber : this.user.phoneNumber;
    this.parent.email = email != '' ? email : this.user.email;

    this.userService.editParent(this.parent).subscribe(_ =>
      this.location.back());
  }

}
