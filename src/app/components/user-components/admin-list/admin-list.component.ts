import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from '../../../../../node_modules/rxjs';
import { PublicUser } from '../../../models/PublicUser';
import { UserService } from '../../../services/user.service';
import { CurrentUser } from '../../../models/CurrentUser';
import { CurrentUserService } from '../../../services/current-user.service';
import { AdminUser } from '../../../models/AdminUser';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {
  users$: Observable<PublicUser[]>;
  currentUser: CurrentUser;
  private searchTerm = new Subject<string>();

  constructor(private userService: UserService, currentUserService: CurrentUserService) {
    this.currentUser = currentUserService.currentUser;
   }

   search(term: string) {
     this.searchTerm.next(term);
   }

  ngOnInit() {
    this.users$ = this.userService.getAdmins();
  }
}
