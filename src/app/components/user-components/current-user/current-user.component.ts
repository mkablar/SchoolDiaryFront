import { Component, OnInit } from '@angular/core';
import { CurrentUser } from '../../../models/CurrentUser';
import { CurrentUserService } from '../../../services/current-user.service';

@Component({
  selector: 'app-current-user',
  templateUrl: './current-user.component.html',
  styleUrls: ['./current-user.component.css']
})
export class CurrentUserComponent implements OnInit {
  currentUser: CurrentUser;

  constructor(private currentUserService: CurrentUserService) { }

  ngOnInit() {
    this.currentUser = this.currentUserService.currentUser;
  }

  login(username, password) {
    this.currentUserService.login(username, password).subscribe(currentUser =>
      this.currentUser = currentUser);
  }

  logout() {
    this.currentUserService.logout();
    this.currentUser = this.currentUserService.currentUser;
  }
}
