import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { CreateAdultUser } from '../../../models/CreateAdultUser';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.css']
})
export class AdminAddComponent implements OnInit {
  admin: CreateAdultUser;

  constructor(private userService: UserService, private router: Router) {
    this.admin = new CreateAdultUser();
   }

  ngOnInit() {
  }

  addAdmin(userName: string, firstName: string, lastName: string, password: string, confirmPassword: string,
     address: string, phoneNumber: string, email: string) {
      this.admin.userName = userName;
      this.admin.firstName = firstName;
      this.admin.lastName = lastName;
      this.admin.password = password;
      this.admin.confirmPassword = confirmPassword;
      this.admin.userRole = 'admins';
      this.admin.address = address;
      this.admin.phoneNumber = phoneNumber;
      this.admin.email = email;

      this.userService.addAdmin(this.admin).subscribe((admin: CreateAdultUser) => {
        this.router.navigate(['/admins']);
      });

      return false;
     }

}
