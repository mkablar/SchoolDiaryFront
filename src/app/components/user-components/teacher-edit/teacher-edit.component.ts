import { Component, OnInit } from '@angular/core';
import { TeacherUser } from '../../../models/TeacherUser';
import { UpdateAdultUser } from '../../../models/UpdateAdultUser';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Location } from '../../../../../node_modules/@angular/common';

@Component({
  selector: 'app-teacher-edit',
  templateUrl: './teacher-edit.component.html',
  styleUrls: ['./teacher-edit.component.css']
})
export class TeacherEditComponent implements OnInit {
  user: TeacherUser;
  teacher: UpdateAdultUser;

  constructor(private userService: UserService, private router: Router, private location: Location,
     private route: ActivatedRoute) {
    this.teacher = new UpdateAdultUser();
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserTeacher(id).subscribe(user => this.user = user);
  }

  editTeacher(id: string, userName: string, firstName: string, lastName: string, address: string,
    phoneNumber: string, email: string) {
      this.teacher.id = id != '' ? id : this.user.id;
      this.teacher.userName = userName != '' ? userName : this.user.userName;
      this.teacher.firstName = firstName != '' ? firstName : this.user.firstName;
      this.teacher.lastName = lastName != '' ? lastName : this.user.lastName;
      this.teacher.address = address != '' ? address : this.user.address;
      this.teacher.phoneNumber = phoneNumber != '' ? phoneNumber : this.user.phoneNumber;
      this.teacher.email = email != '' ? email : this.user.email;

      this.userService.editTeacher(this.teacher).subscribe(_ =>
        this.location.back());
  }
}
