import { Component, OnInit } from '@angular/core';
import { CreateAdultUser } from '../../../models/CreateAdultUser';
import { UserService } from '../../../services/user.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-parent-add',
  templateUrl: './parent-add.component.html',
  styleUrls: ['./parent-add.component.css']
})
export class ParentAddComponent implements OnInit {
  parent: CreateAdultUser;

  constructor(private userService: UserService, private router: Router) {
    this.parent = new CreateAdultUser();
   }

  ngOnInit() {
  }

  addParent(userName: string, firstName: string, lastName: string, password: string, confirmPassword: string,
    address: string, phoneNumber: string, email: string) {
     this.parent.userName = userName;
     this.parent.firstName = firstName;
     this.parent.lastName = lastName;
     this.parent.password = password;
     this.parent.confirmPassword = confirmPassword;
     this.parent.userRole = 'parents';
     this.parent.address = address;
     this.parent.phoneNumber = phoneNumber;
     this.parent.email = email;

     this.userService.addParent(this.parent).subscribe((parent: CreateAdultUser) => {
       this.router.navigate(['/parents']);
     });

     return false;
    }

}
