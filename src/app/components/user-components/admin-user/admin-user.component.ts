import { Component, OnInit, Input } from '@angular/core';
import { AdminUser } from '../../../models/AdminUser';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {
  user: AdminUser;

  constructor(private route: ActivatedRoute, private userService: UserService, private location: Location) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserAdmin(id).subscribe(user => this.user = user);
  }

}
