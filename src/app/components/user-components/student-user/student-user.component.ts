import { Component, OnInit, Input } from '@angular/core';
import { StudentUser } from '../../../models/StudentUser';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { UserService } from '../../../services/user.service';
import { Location } from '../../../../../node_modules/@angular/common';
import { Observable } from '../../../../../node_modules/rxjs';
import { StudentSubjectMarks } from '../../../models/StudentSubjectMarks';
import { MarkService } from '../../../services/mark.service';

@Component({
  selector: 'app-student-user',
  templateUrl: './student-user.component.html',
  styleUrls: ['./student-user.component.css']
})
export class StudentUserComponent implements OnInit {
  @Input() user: StudentUser;

  constructor(private route: ActivatedRoute, private userService: UserService, private location: Location) {}

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserStudent(id).subscribe(user => this.user = user);
  }

}
