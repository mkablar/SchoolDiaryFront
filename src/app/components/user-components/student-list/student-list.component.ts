import { Component, OnInit } from '@angular/core';
import { Observable } from '../../../../../node_modules/rxjs';
import { PublicUser } from '../../../models/PublicUser';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  users$: Observable<PublicUser[]>;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.users$ = this.userService.getStudents();
  }

}
