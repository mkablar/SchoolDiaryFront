import { Component, OnInit, Input } from '@angular/core';
import { TeacherUser } from '../../../models/TeacherUser';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Location } from '../../../../../node_modules/@angular/common';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-teacher-user',
  templateUrl: './teacher-user.component.html',
  styleUrls: ['./teacher-user.component.css']
})
export class TeacherUserComponent implements OnInit {
  @Input() user: TeacherUser;

  constructor(private route: ActivatedRoute, private userService: UserService, private location: Location) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserTeacher(id).subscribe(user => this.user = user);
  }
}
