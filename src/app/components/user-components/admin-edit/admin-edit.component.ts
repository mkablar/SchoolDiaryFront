import { Component, OnInit, Input } from '@angular/core';
import { AdminUser } from '../../../models/AdminUser';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { UpdateAdultUser } from '../../../models/UpdateAdultUser';
import { Location } from '../../../../../node_modules/@angular/common';

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {
  user: AdminUser;
  admin: UpdateAdultUser;

  constructor(private userService: UserService, private router: Router, private location: Location,
     private route: ActivatedRoute) {
    this.admin = new UpdateAdultUser();
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserAdmin(id).subscribe(user => this.user = user);
  }

  editAdmin(id: string, userName: string, firstName: string, lastName: string, address: string,
    phoneNumber: string, email: string) {
      this.admin.id = id != '' ? id : this.user.id;
      this.admin.userName = userName != '' ? userName : this.user.userName;
      this.admin.firstName = firstName != '' ? firstName : this.user.firstName;
      this.admin.lastName = lastName != '' ? lastName : this.user.lastName;
      this.admin.address = address != '' ? address : this.user.address;
      this.admin.phoneNumber = phoneNumber != '' ? phoneNumber : this.user.phoneNumber;
      this.admin.email = email != '' ? email : this.user.email;

      this.userService.editAdmin(this.admin).subscribe(_ =>
        this.location.back());
  }

}
