import { Component, OnInit } from '@angular/core';
import { StudentUser } from '../../../models/StudentUser';
import { UpdateStudent } from '../../../models/UpdateStudent';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Location } from '../../../../../node_modules/@angular/common';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {
  user: StudentUser;
  student: UpdateStudent;

  constructor(private userService: UserService, private router: Router, private location: Location,
    private route: ActivatedRoute) {
    this.student = new UpdateStudent();
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserStudent(id).subscribe(user => this.user = user);
  }

  editStudent(id: string, userName: string, firstName: string, lastName: string, address: string) {
    this.student.id = id != '' ? id : this.user.id;
    this.student.userName = userName != '' ? userName : this.user.userName;
    this.student.firstName = firstName != '' ? firstName : this.user.firstName;
    this.student.lastName = lastName != '' ? lastName : this.user.lastName;
    this.student.address = address != '' ? address : this.user.address;

    this.userService.editStudent(this.student).subscribe(_ =>
      this.location.back());
  }
}
