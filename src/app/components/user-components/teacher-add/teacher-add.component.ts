import { Component, OnInit } from '@angular/core';
import { CreateAdultUser } from '../../../models/CreateAdultUser';
import { UserService } from '../../../services/user.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-teacher-add',
  templateUrl: './teacher-add.component.html',
  styleUrls: ['./teacher-add.component.css']
})
export class TeacherAddComponent implements OnInit {
  teacher: CreateAdultUser;

  constructor(private userService: UserService, private router: Router) {
    this.teacher = new CreateAdultUser();
   }

  ngOnInit() {
  }

  addTeacher(userName: string, firstName: string, lastName: string, password: string, confirmPassword: string,
    address: string, phoneNumber: string, email: string) {
     this.teacher.userName = userName;
     this.teacher.firstName = firstName;
     this.teacher.lastName = lastName;
     this.teacher.password = password;
     this.teacher.confirmPassword = confirmPassword;
     this.teacher.userRole = 'teachers';
     this.teacher.address = address;
     this.teacher.phoneNumber = phoneNumber;
     this.teacher.email = email;

     this.userService.addTeacher(this.teacher).subscribe((teacher: CreateAdultUser) => {
       this.router.navigate(['/teachers']);
     });

     return false;
    }
}
