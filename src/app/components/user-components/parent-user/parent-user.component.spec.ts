import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentUserComponent } from './parent-user.component';

describe('ParentUserComponent', () => {
  let component: ParentUserComponent;
  let fixture: ComponentFixture<ParentUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
