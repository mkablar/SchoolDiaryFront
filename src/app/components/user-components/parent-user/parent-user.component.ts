import { Component, OnInit, Input } from '@angular/core';
import { ParentUser } from '../../../models/ParentUser';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-parent-user',
  templateUrl: './parent-user.component.html',
  styleUrls: ['./parent-user.component.css']
})
export class ParentUserComponent implements OnInit {
  @Input() user: ParentUser;

  constructor(private route: ActivatedRoute, private userService: UserService, private location: Location) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserParent(id).subscribe(user => this.user = user);
  }

}
