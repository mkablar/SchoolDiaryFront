import { Component, OnInit, Input } from '@angular/core';
import { Observable } from '../../../../node_modules/rxjs';
import { MarkService } from '../../services/mark.service';
import { StudentUser } from '../../models/StudentUser';
import { StudentSubjectMarks } from '../../models/StudentSubjectMarks';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { UserService } from '../../services/user.service';
import { Location } from '../../../../node_modules/@angular/common';

@Component({
  selector: 'app-mark-list',
  templateUrl: './mark-list.component.html',
  styleUrls: ['./mark-list.component.css']
})
export class MarkListComponent implements OnInit {
  user: StudentUser;
  marks$: Observable<StudentSubjectMarks[]>;

  constructor(private route: ActivatedRoute, private userService: UserService, private location: Location,
    private markService: MarkService) { }

  ngOnInit() {
    this.getUser();
    this.marks$ = this.markService.getMarks(this.route.snapshot.paramMap.get('id'));
  }

  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUserStudent(id).subscribe(user => this.user = user);
  }

}
