import { Component, OnInit, Input } from '@angular/core';
import { Subject } from '../../models/Subject';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';
import { SubjectService } from '../../services/subject.service';
import { Location } from '../../../../node_modules/@angular/common';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css']
})
export class SubjectDetailComponent implements OnInit {
  @Input() subject: Subject;

  constructor(private route: ActivatedRoute, private subjectService: SubjectService, private location: Location) { }

  ngOnInit() {
    this.getSubject();
  }

  getSubject(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.subjectService.getSubject(id).subscribe(subject => this.subject = subject);
  }

}
