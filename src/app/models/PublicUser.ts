export class PublicUser {
    id: string;
    firstName: string;
    lastName: string;
}