export class UpdateAdultUser {
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    phoneNumber: string;
    email: string;
    address: string;
}