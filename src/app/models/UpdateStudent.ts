export class UpdateStudent {
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    address: string;
}