export class CreateSubject {
    name: string;
    description: string;
    weeklySubjectFund: number;
}
