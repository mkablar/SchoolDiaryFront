export class CreateStudent {
    firstName: string;
    lastName: string;
    userName: string;
    address: string;
    password: string;
    confirmPassword: string;
    userRole: string;
}
