export class CreateAdultUser {
    firstName: string;
    lastName: string;
    userName: string;
    phoneNumber: string;
    email: string;
    address: string;
    password: string;
    confirmPassword: string;
    userRole: string;
}
