import { PublicUser } from './PublicUser';
import { Subject } from './Subject';
import { PublicMark } from './PublicMark';

export class StudentSubjectMarks {
    id: number;
    student: PublicUser;
    teacher: PublicUser;
    subject: Subject;
    marks: PublicMark[];
}
