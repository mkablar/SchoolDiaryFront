import { PublicUser } from './PublicUser';
import { Subject } from './Subject';

export class Mark {
    id: number;
    description: string;
    markDate: Date;
    student: PublicUser;
    grader: PublicUser;
    subject: Subject;
}
