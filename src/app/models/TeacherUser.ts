import { Subject } from './Subject';

export class TeacherUser {
    id: string;
    userName: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    address: string;
    subjects: Subject[];
}
