import { StudentUser } from './StudentUser';

export class ParentUser {
    id: string;
    userName: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    address: string;
    students: StudentUser[];
}
