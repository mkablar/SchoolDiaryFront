import { ParentUser } from './ParentUser';

export class StudentUser {
    id: string;
    userName: string;
    firstName: string;
    lastName: string;
    address: string;
    parents: ParentUser[];
}
