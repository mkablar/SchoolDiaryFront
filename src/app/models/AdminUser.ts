export class AdminUser {
    id: string;
    userName: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    address: string;
}
