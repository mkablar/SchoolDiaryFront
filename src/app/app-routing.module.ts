import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentUserComponent } from './components/user-components/current-user/current-user.component';
import { LoginComponent } from './components/login/login.component';
import { AdminListComponent } from './components/user-components/admin-list/admin-list.component';
import { TeacherListComponent } from './components/user-components/teacher-list/teacher-list.component';
import { ParentListComponent } from './components/user-components/parent-list/parent-list.component';
import { StudentListComponent } from './components/user-components/student-list/student-list.component';
import { AdminUserComponent } from './components/user-components/admin-user/admin-user.component';
import { TeacherUserComponent } from './components/user-components/teacher-user/teacher-user.component';
import { ParentUserComponent } from './components/user-components/parent-user/parent-user.component';
import { StudentUserComponent } from './components/user-components/student-user/student-user.component';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectDetailComponent } from './components/subject-detail/subject-detail.component';
import { MarkListComponent } from './components/mark-list/mark-list.component';
import { MarkDetailComponent } from './components/mark-detail/mark-detail.component';
import { AdminAddComponent } from './components/user-components/admin-add/admin-add.component';
import { TeacherAddComponent } from './components/user-components/teacher-add/teacher-add.component';
import { ParentAddComponent } from './components/user-components/parent-add/parent-add.component';
import { StudentAddComponent } from './components/user-components/student-add/student-add.component';
import { SubjectAddComponent } from './components/subject-add/subject-add.component';
import { MarkAddComponent } from './components/mark-add/mark-add.component';
import { AdminEditComponent } from './components/user-components/admin-edit/admin-edit.component';
import { ParentEditComponent } from './components/user-components/parent-edit/parent-edit.component';
import { TeacherEditComponent } from './components/user-components/teacher-edit/teacher-edit.component';
import { StudentEditComponent } from './components/user-components/student-edit/student-edit.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'currentUser', component: CurrentUserComponent},
  { path: 'admins', component: AdminListComponent },
  { path: 'teachers', component: TeacherListComponent },
  { path: 'parents', component: ParentListComponent },
  { path: 'students', component: StudentListComponent },
  { path: 'subjects', component: SubjectListComponent },
  { path: 'students/:id/getmarks', component: MarkListComponent },
  { path: 'admins/:id', component: AdminUserComponent },
  { path: 'teachers/:id', component: TeacherUserComponent },
  { path: 'parents/:id', component: ParentUserComponent },
  { path: 'students/:id', component: StudentUserComponent },
  { path: 'subjects/:id', component: SubjectDetailComponent },
  { path: 'marks/:id', component: MarkDetailComponent },
  { path: 'addAdmin', component: AdminAddComponent },
  { path: 'addTeacher', component: TeacherAddComponent },
  { path: 'addParent', component: ParentAddComponent },
  { path: 'addStudent', component: StudentAddComponent },
  { path: 'addSubject', component: SubjectAddComponent },
  { path: 'addMark', component: MarkAddComponent },
  { path: 'editAdmin/:id', component: AdminEditComponent },
  { path: 'editParent/:id', component: ParentEditComponent },
  { path: 'editTeacher/:id', component: TeacherEditComponent },
  { path: 'editStudent/:id', component: StudentEditComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
