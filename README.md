# School Diary FrontEnd

FrontEnd for Government of Republic of Serbia sponsored retraining course.

## Built With

* Angular 6
* TypeScript
* Bootstrap

## Author

* **Milan Kablar** - [mkablar](https://gitlab.com/mkablar)

## Acknowledgments

Thank you for all the lessons and help:

* [Nikola Todorović](https://github.com/TodorovicNikola)